package com.abdeljalilkchih.palestine.Fragment;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.abdeljalilkchih.palestine.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdView;
import yalantis.com.sidemenu.interfaces.ScreenShotable;

public class FragmentOne extends Fragment implements ScreenShotable {

    private AdView MyAdView2;
    private View Fragmentone_view;
    private Bitmap bitmap;


    public static FragmentOne newInstance() {
        FragmentOne fragmentOne = new FragmentOne();
        return fragmentOne;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);
   MobileAds.initialize(this.getContext(), "ca-app-pub-9961705383410701~3103812044");
        MyAdView2 = rootView.findViewById(R.id.MyAdView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        MyAdView2.loadAd(adRequest);
        WebView webView;
        webView=(WebView) rootView.findViewById(R.id.Web_View2);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());


        //ربط الفيو بالصفحات
        webView.loadUrl("file:///android_asset/webfiles/index.html");
        return rootView;
    }



    @Override
    public void takeScreenShot() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = Bitmap.createBitmap(Fragmentone_view.getWidth(),
                        Fragmentone_view.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                Fragmentone_view.draw(canvas);
                FragmentOne.this.bitmap = bitmap;
            }
        };

        thread.start();

    }

    @Override
    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.Fragmentone_view = view.findViewById(R.id.container);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}